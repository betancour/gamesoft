const Score = require('../models/score.model');

const scoreCtrl= {};

scoreCtrl.getScores = async (req, res, next) => {
    const score = await Score.find();
    res.json(score);
};

scoreCtrl.createScore = async (req, res, next) => {
    const score = new Score({
        team: req.body.team,
        game: req.body.game,
        date: req.body.date,
        points: req.body.points,
    });
    await score.save();
    res.json({status: 'Score created'});
}; 

scoreCtrl.getScore = async (req, res, next) => {
    const { id } = req.params;
    const score = await Score.findById(id);
    res.json(score);
};

scoreCtrl.editScore = async (req, res, next) => {
    const { id } = req.params;
    const score = {
        team: req.body.team,
        game: req.body.game,
        date: req.body.date,
        points: req.body.points
    };
    await Score.findByIdAndUpdate(id, {$set: score}, {new: true});
    res.json({status: 'Score Updated'});
};

scoreCtrl.deleteScore = async (req, res, next) => {
await Score.findByIdAndRemove(req.params.id);
    res.json({status: 'Score Deleted'});
};

module.exports = scoreCtrl;
