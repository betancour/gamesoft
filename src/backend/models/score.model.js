const mongoose = require('mongoose');
const { Schema } = mongoose;

const scoreSchema = new Schema({
    team: { type: String, require: true},
    game: { type: Number, required: true},
    date: { type: String, required: true},
    points: { type: Number, required: true},
});

module.exports = mongoose.model('Score', scoreSchema);
