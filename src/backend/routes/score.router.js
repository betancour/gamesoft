const express = require('express');
const router = express.Router();

const score = require('../controllers/score.controller.js');

router.get('/', score.getScores);
router.post('/', score.createScore);
router.get('/:id', score.getScore);
router.put('/:id', score.editScore);
router.delete('/:id', score.deleteScore);

module.exports = router;
